<?php
namespace Seller\Listing\Controller\Adminhtml;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Seller\Listing\Model\ContactFactory;
abstract class  News extends \Magento\Backend\App\Action
{
/**
* Core registry
*
* @var \Magento\Framework\Registry
*/
protected $_coreRegistry;
/**
* Result page factory
*
* @var \Magento\Framework\View\Result\PageFactory
*/
protected $_resultPageFactory;
/**
* News model factory
*
* @var \Seller\Listing\Model\ContactFactory
*/
protected $_modelContactFactory;
/**
* @param Context $context
* @param Registry $coreRegistry
* @param PageFactory $resultPageFactory
* @param ContactFactory $modelContactFactory
*/
public function __construct(
Context $context,
Registry $coreRegistry,
PageFactory $resultPageFactory,
ContactFactory $modelContactFactory
) {
parent::__construct($context);
$this->_coreRegistry = $coreRegistry;
$this->_resultPageFactory = $resultPageFactory;
$this->_modelContactFactory = $modelContactFactory;
}
/**
* News access rights checking
*
* @return bool
*/
protected function _isAllowed()
{
return $this->_authorization->isAllowed('Seller_Listing::manage_news');
}
}