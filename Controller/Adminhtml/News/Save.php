<?php
namespace Seller\Listing\Controller\Adminhtml\News;
use Seller\Listing\Controller\Adminhtml\News;
class Save extends News
{
/**
* @return void
*/
public function execute()
{
	$isPost = $this->getRequest()->getPost();
	if ($isPost) {
		$post = $this->getRequest()->getPostValue();

	   $comp_name=$post['comp_name'];
         $comp_email=$post['comp_email'];
         $comp_mobile=$post['comp_mobile'];
         $comp_city=$post['comp_city'];
         $comp_pincode=$post['comp_pincode'];
         $comp_pan=$post['comp_pan'];
         $comp_gst=$post['comp_gst'];
         $comp_terms=$post['comp_terms'];

	    
	    
	    
	  
	         //$emp_joindate=$post['emp_joindate'];

	$newsModel = $this->_modelContactFactory->create();
	$newsId = $this->getRequest()->getParam('id');

	//echo"<BR>==".$newsId;
	if ($newsId) {
	$newsModel->load($newsId);
	}

	$formData = $this->getRequest()->getParam('news');

	//print_r($formData);
	//exit();

	 $newsModel->addData([
	       "comp_name" => "$comp_name",
            "comp_email" => "$comp_email",
            "comp_mobile" => "$comp_mobile",
            "comp_city" => "$comp_city",
            "comp_pincode" => "$comp_pincode",
            "comp_pan" => "$comp_pan",
            "comp_gst" => "$comp_gst",
            "comp_terms" => "$comp_terms"

	        ]);


	$newsModel->setData($formData);
	try {
	// Save news
	$newsModel->save();
	// Display success message
	$this->messageManager->addSuccess(__('The news has been saved.'));
	// Check if 'Save and Continue'
	if ($this->getRequest()->getParam('back')) {
	$this->_redirect('*/*/edit', ['id' => $newsModel->getId(), '_current' => true]);
	return;
	}
	// Go to grid page
	$this->_redirect('*/*/');
	return;
	} 
	catch (\Exception $e) 
	{
	$this->messageManager->addError($e->getMessage());
	}
	$this->_getSession()->setFormData($formData);
	$this->_redirect('*/*/edit', ['id' => $newsId]);
	}
	}
}