<?php
namespace Seller\Listing\Controller\Adminhtml\News;
use Seller\Listing\Controller\Adminhtml\News;
class NewAction extends News
{
/**
* Create new news action
*
* @return void
*/
public function execute()
{
$this->_forward('edit');
}
}