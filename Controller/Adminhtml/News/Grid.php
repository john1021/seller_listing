<?php
namespace Seller\Listing\Controller\Adminhtml\News;
use Seller\Listing\Controller\Adminhtml\News;
class Grid extends News
{
/**
* @return void
*/
public function execute()
{
return $this->_resultPageFactory->create();
}
}