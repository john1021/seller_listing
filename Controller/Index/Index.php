<?php
/**
 *
 * Copyright © 2015 Employeecommerce. All rights reserved.****************
 */
namespace Seller\Listing\Controller\Index;

use Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use Seller\Listing\Model\ContactFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_modelContactFactory;
	
	
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		ContactFactory $modelContactFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->_messageManager = $messageManager;
		$this->_modelContactFactory = $modelContactFactory;
    }
    
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
		 	/* Starts Process For Base URL */
	         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
             $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
             $baseUrl = $storeManager->getStore()->getBaseUrl();	
			 
			 $red_url=$baseUrl."listing/"."display/display";
	        /* Finish Process For Base URL */

         $post = $this->getRequest()->getPostValue();

         //print_r($post);
         //exit();
         if(isset($post) && !empty($post)) {

         $comp_name=$post['comp_name'];
         $comp_email=$post['comp_email'];
         $comp_mobile=$post['comp_mobile'];
         $comp_city=$post['comp_city'];
         $comp_pincode=$post['comp_pincode'];
         $comp_pan=$post['comp_pan'];
         $comp_gst=$post['comp_gst'];
         $comp_terms=$post['comp_terms'];

        $contact = $this->_objectManager->create('Seller\Listing\Model\Contact');
        $contact->addData([
            "comp_name" => "$comp_name",
            "comp_email" => "$comp_email",
            "comp_mobile" => "$comp_mobile",
            "comp_city" => "$comp_city",
            "comp_pincode" => "$comp_pincode",
            "comp_pan" => "$comp_pan",
            "comp_gst" => "$comp_gst",
            "comp_terms" => "$comp_terms"
        ]);

        $saveData = $contact->save();

        if($saveData) {
           $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
           $resultRedirect->setUrl("$red_url");
           return $resultRedirect;
          } else {
           $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
          $resultRedirect->setUrl("$red_url");
          }
       
	   
	   
	
	   
        
		
      
       }
	   
	    $this->resultPage = $this->resultPageFactory->create();  
         return $this->resultPage;
      
	  
	  
	  /*$post = $this->_postFactory->create();
		$collection = $post->getCollection();
		foreach($collection as $item){
			echo "<pre>";
			print_r($item->getData());
			echo "</pre>";
		}
		exit();
		return $this->_pageFactory->create();*/

       
     
    }
}
