<?php
/**
 * Copyright © 2015 Seller . All rights reserved.
 */
namespace Seller\Listing\Block\Pagelisting;
class Pagelisting extends \Magento\Framework\View\Element\Template
{
   /**
    * @var \Tutorial\SimpleNews\Model\NewsFactory
    */
   protected $_modelContactFactory;

   /**
    * @param Template\Context $context
    * @param NewsFactory $newsFactory
    * @param array $data
    */
   public function __construct(
      \Magento\Framework\View\Element\Template\Context $context,
		\Seller\Listing\Model\ContactFactory $modelContactFactory,
      array $data = []
   ) {
        $this->_modelContactFactory = $modelContactFactory;
        parent::__construct($context, $data);
   }

   /**
     * Set news collection
     */
    protected  function _construct()
    {
        parent::_construct();
        $collection = $this->_modelContactFactory->create()->getCollection()->setOrder('id', 'DESC');
        $this->setCollection($collection);

    }

   /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
           'Magento\Theme\Block\Html\Pager','listing.pagelisting.pager'
        );
        $pager->setLimit(5)
            ->setShowAmounts(false)
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();

        return $this;
    }

   /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}

