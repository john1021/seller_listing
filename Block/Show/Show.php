<?php
/**
 * Copyright © 2015 Seller . All rights reserved.
 */
namespace Seller\Listing\Block\Show;
class Show extends \Magento\Framework\View\Element\Template
{
	protected $_modelContactFactory;
	protected $_customerSession;
	
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Seller\Listing\Model\ContactFactory $modelContactFactory,
		 \Magento\Customer\Model\SessionFactory $customerSession,
        array $data = []
	)
	{
		$this->_modelContactFactory = $modelContactFactory;
		 $this->_customerSession = $customerSession->create();
		parent::__construct($context);
	}

	
	 public function getLoggedinCustomerId() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getId();
        }
        return false;
    }
 
    public function getCustomerData() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData();
        }
        return false;
    }

	public function getConatctCollection(){
	    $ContactModel = $this->_modelContactFactory->create();
        return $ContactModel->getCollection();
	}
}



