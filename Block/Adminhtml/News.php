<?php
/**
 * Copyright © 2015 Seller . All rights reserved.
 */
namespace Seller\Listing\Block\Adminhtml;
class News extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'Seller_Listing';
        $this->_headerText = __('Manage Seller Listing');
        $this->_addButtonLabel = __('Add Seller');
        parent::_construct();
	}
}

