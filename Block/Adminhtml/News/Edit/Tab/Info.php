<?php
namespace Seller\Listing\Block\Adminhtml\News\Edit\Tab;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Seller\Listing\Model\System\Config\Status;
class Info extends Generic implements TabInterface
{
/**
* @var \Magento\Cms\Model\Wysiwyg\Config
*/
protected $_wysiwygConfig;
/**
* @var \Tutorial\SimpleNews\Model\Config\Status
*/
protected $_newsStatus;
/**
* @param Context $context
* @param Registry $registry
* @param FormFactory $formFactory
* @param Config $wysiwygConfig
* @param Status $newsStatus
* @param array $data
*/
public function __construct(
Context $context,
Registry $registry,
FormFactory $formFactory,
Config $wysiwygConfig,
Status $newsStatus,
array $data = []
) {
$this->_wysiwygConfig = $wysiwygConfig;
$this->_newsStatus = $newsStatus;
parent::__construct($context, $registry, $formFactory, $data);
}
/**
* Prepare form fields
*
* @return \Magento\Backend\Block\Widget\Form
*/
protected function _prepareForm()
{
/** @var $model \Seller\Listing\Model\Contact */
$model = $this->_coreRegistry->registry('listing_news');
/** @var \Magento\Framework\Data\Form $form */
$form = $this->_formFactory->create();
$form->setHtmlIdPrefix('');
$form->setFieldNameSuffix('');
$fieldset = $form->addFieldset(
'base_fieldset',
['legend' => __('General')]
);
if ($model->getId()) {
$fieldset->addField('id','hidden',['name' => 'id']);
}

$fieldset->addField('comp_name','text',['name' => 'comp_name','label' => __('Company Name'),'required' => true]);
$fieldset->addField('comp_email','text',['name' => 'comp_email','label' => __('Company Email'),'required' => true]);


$fieldset->addField('comp_mobile','text',['name' => 'comp_mobile','label' => __('Company Mobile No.'),'required' => true]);
$fieldset->addField('comp_city','text',['name' => 'comp_city','label' => __('Company City'),'required' => true]);

$fieldset->addField('comp_pincode','text',['name' => 'comp_pincode','label' => __('Company Pincode No.'),'required' => true]);
$fieldset->addField('comp_pan','text',['name' => 'comp_pan','label' => __('Company Pan No.'),'required' => true]);
$fieldset->addField('comp_gst','text',['name' => 'comp_gst','label' => __('Company GST No.'),'required' => true]);






$fieldset->addField('comp_terms', 'checkboxes', array('label' => 'I have read Terms Condition', 'name' => 'comp_terms',
'values' => array(
    array('value'=>'Yes', 'label'=>'Terms'),
   // array('value'=>'2', 'label'=>'Paypal'),
   // array('value'=>'3', 'label'=>'Authorize.Net'),
    //array('value'=>'4', 'label'=>'Square'),
),
        'required' => true,
        //'checked' => array('1','1'),

));


$data = $model->getData();
$form->setValues($data);
$this->setForm($form);
return parent::_prepareForm();
}
/**
* Prepare label for tab
*
* @return string
*/
public function getTabLabel()
{
return __('News Info');
}
/**
* Prepare title for tab
*
* @return string
*/
public function getTabTitle()
{
return __('News Info');
}
/**
* {@inheritdoc}
*/
public function canShowTab()
{
return true;
}
/**
* {@inheritdoc}
*/
public function isHidden()
{
return false;
}
}