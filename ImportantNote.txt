Video URL = https://www.screencast.com/t/3JVwkV5w

[1] Seller's Company Registration Form 
[2] Registration Form having field Company Name, Email , Mobile, City, Pincode, Pan No & GST No.
[3] Frontend Company Registration Form  URL =YourwebsiteURL/listing/index/index
As per Video My script running as Local Machine
http://127.0.0.1/magento23/listing/index/index

[4] Login in Admin End 
[5] Admin End Listing of All Seller's Listing
[6] Left hand side need to click Seller Listing, redirect on all Seller's Listing  page

[7] Click any listing row to edit seller's details

[8] Click on Add Seller ,admin can also add Seller's Company details along with edit / delete
[9] To Delete any seller's record click on any checkbox, & select Delete option, you delete
    either single record or multiple records  
[10] How to install
     Unzip what file I have provided & put inside 
     app/code
     Run following below command

     php bin/magento setup:upgrade

 
php bin/magento setup:static-content:deploy

 
php bin/magento indexer:reindex

 
php bin/magento cache:clean


php bin/magento cache:flush  
